﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    private GameMaster gm;
    public GameObject player;
    public AudioClip death;

    // Use this for initialization
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            player.transform.localPosition = gm.lastCheckPointPos;
            AudioSource.PlayClipAtPoint(death, gm.lastCheckPointPos);
        }

    }

}
