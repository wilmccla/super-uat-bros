﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class King : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other) // When player enters the trigger
    {
        Destroy(other.gameObject); // Destory the Gameobject
        SceneManager.LoadScene("Battle"); // Load the battle scene
    }
}
