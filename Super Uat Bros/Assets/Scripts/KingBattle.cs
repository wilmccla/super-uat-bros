﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KingBattle : MonoBehaviour
{
    public Animator KingAnimator;
    public static float KingHP = 100;
    public static float KingMaxHP = 100;
    public static Image KHealthBar;
    public float KingAttack = 15;
    public AudioClip hit;
    public AudioClip death;

    void Start()
    {
        KHealthBar = GameObject.FindGameObjectWithTag("KHP").GetComponent<Image>();
    }

    void Update()
    {
        if (BattleManager.Turn == 2)
        {
            KingAnimator.SetTrigger("Attack");
            AudioSource.PlayClipAtPoint(hit, new Vector3(0, 0, 0));
            PlayerBattle.TakeDamage(KingAttack);
        }

        if (KingHP <= 0)
        {
            Destroy(gameObject);
            AudioSource.PlayClipAtPoint(death, new Vector3(0, 0, 0));
            SceneManager.LoadScene("Level 4");
        }
    }

    public static void TakeDamage(float damage)
    {
        KingHP -= damage;
        KHealthBar.fillAmount = KingHP / KingMaxHP;
        Debug.Log("King HP " + KingHP);
        BattleManager.Turn = 2;
    }
}
