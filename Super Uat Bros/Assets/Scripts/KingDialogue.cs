﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KingDialogue : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    public float typeSpeed;
    public GameObject UI;
    public GameObject Button;

    void Start()
    {
        UI.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            NextSentence();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Button.SetActive(true);
        UI.SetActive(true);
        StartCoroutine(Type());
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typeSpeed);
        }
    }

    public void NextSentence()
    {
        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            UI.SetActive(false);
        }
    }
}
