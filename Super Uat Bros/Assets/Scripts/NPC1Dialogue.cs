﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NPC1Dialogue : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    public float typeSpeed;
    public GameObject UI;
    public GameObject Button;

    void Start()
    {
        UI.SetActive(false); // Sets UI to not be active when the scene is loaded
    }

    void OnTriggerEnter2D(Collider2D other) // When player enters trigger
    {
        UI.SetActive(true); // UI will be visible
        StartCoroutine(Type()); // Start Coroutine Type
    }

    IEnumerator Type() // Types out every letter one by one
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typeSpeed);
        }
    }

    public void NextSentence() // Goes to the next sentence in the array
    {
        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else // When all sentences are done, disable UI and the button
        {
            UI.SetActive(false);
            Button.SetActive(false);
        }
    }
}
