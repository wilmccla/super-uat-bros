﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NPC2Dialogue : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    public float typeSpeed;
    public GameObject UI;
    public GameObject Button;

    void Start()
    {
        Button.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Button.SetActive(true);
        UI.SetActive(true);
        textDisplay.text = "";
        StartCoroutine(Type());
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typeSpeed);
        }
    }

    public void NextSentence()
    {
        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            UI.SetActive(false);
            Button.SetActive(false);
        }
    }
}
