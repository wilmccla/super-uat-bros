﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayAudio : MonoBehaviour
{

    public AudioClip clip;

	void OnTriggerEnter2D (Collider2D other) {
		AudioSource.PlayClipAtPoint(clip, new Vector2(6.5f, 11), 0.01f);
	}

}
