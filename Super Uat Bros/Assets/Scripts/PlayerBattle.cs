﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBattle : MonoBehaviour
{
    public GameObject player;
    public Animator swordAnimator;
    public static float PlayerHP = 100;
    public static float PlayerMaxHP = 100;
    public static Image HealthBar;
    public float PlayerAttack = 10;
    public AudioClip hit;

    void Start()
    {
        HealthBar = GameObject.FindGameObjectWithTag("PHP").GetComponent<Image>();
    }

	// Update is called once per frame
	void Update ()
    {
        if (PlayerHP > 100)
        {
            PlayerHP = 100;
        }

        if (PlayerHP <= 0)
        {
            SceneManager.LoadScene("Defeat");
        }
    }

    public void DoAttack()
    {
        if (BattleManager.Turn == 1)
        {
            player.transform.position = new Vector2(4.29f, -1.69f);
            swordAnimator.SetTrigger("SwordAttack");
            AudioSource.PlayClipAtPoint(hit, new Vector3(0, 0 ,0));
        }
    }

    public void DoHeal()
    {
        if (BattleManager.Turn == 1)
        {
            PlayerHP += 50;
            HealthBar.fillAmount = PlayerHP / PlayerMaxHP;
            BattleManager.Turn = 2;
        }
    }

    void Return()
    {
        player.transform.position = new Vector2(-7.55f, -1.69f);
        KingBattle.TakeDamage(10);
    }

    public static void TakeDamage(float damage)
    {
        PlayerHP -= damage;
        HealthBar.fillAmount = PlayerHP / PlayerMaxHP;
        Debug.Log("Player HP " + PlayerBattle.PlayerHP);
        BattleManager.Turn = 1;
    }
}
