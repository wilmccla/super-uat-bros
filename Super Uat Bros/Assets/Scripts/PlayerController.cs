﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator animator; // Declare an animator in order to use animations
    public GameMaster gm;  // Declare GameMaster
    public Rigidbody2D rigidBody; // Declare Rigidbody2D

    // Declaring the Variables
    public float moveSpeed = 3f;
    public float jumpSpeed = 3f;
    public float velocityX;
    public float velocityY;
    public bool facingRight = true;
    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask theGround;

    public float Jumps;
    private float NumJumps;

    
	// Use this for initialization
	void Start ()
	{//
	    rigidBody = GetComponent<Rigidbody2D>();  // rigidBody is able to get velocity values in order to move the sprite
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>(); // Initializing the GameMaster
        NumJumps = Jumps;
    }

    // Update is called once per frame
    void Update ()
	{   // Getting input from the player. Horizontal class applies to both A and D on the keyboard, as well as the left and right arrowkeys.
        if (!Input.GetKey(KeyCode.LeftShift)) velocityX = Input.GetAxisRaw("Horizontal");
        velocityY = rigidBody.velocity.y;
        if (!Input.GetKey(KeyCode.LeftShift)) rigidBody.velocity = new Vector2(velocityX * moveSpeed, velocityY);

        // Give the animator a variable to change in order to transition from an idle to a running animation
        animator.SetFloat("Speed", Mathf.Abs(velocityX));

        // Give the player the ability to jump
	    isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, theGround); //Checks if the player is touching the ground
        animator.SetBool("isGrounded", isGrounded); // Gives the animator a parameter to change animations

        if (isGrounded == true) // If the player is grounded
        {
            NumJumps = Jumps; // NumJumps is equal to Jumps
        }

        if ((Input.GetKeyDown(KeyCode.Space) && NumJumps > 0) || (Input.GetKeyDown(KeyCode.W) && NumJumps > 0)) // If player presses space or up W and they have jumps
        {
            rigidBody.velocity = Vector2.up * jumpSpeed; // The player will jump
            NumJumps--;
        } else if ((Input.GetKeyDown(KeyCode.Space) && NumJumps == 0 && isGrounded == true) || (Input.GetKeyDown(KeyCode.W) && NumJumps == 0 && isGrounded == true)) // Else if the player presses space or W, they have no extra jumps, and they're grounded
        {
            rigidBody.velocity = Vector2.up * jumpSpeed; // The player will jump
        }


        if (Input.GetKeyDown(KeyCode.P)) // If user presses space...
        {
            transform.localPosition = gm.lastCheckPointPos; // Move the player to the last checkpoint
        }

    }

    void LateUpdate()
    {// Determining which way the sprite is moving, and changing the X scale to match the direction. Right is positive, Left is Negative.
        Vector3 scaleX = transform.localScale;
        if (velocityX > 0) // If the velocityX of the player is positive....
        {
            facingRight = true;
        } else if (velocityX < 0) // ... the player is facing right
        {
            facingRight = false;
        }

        if (((facingRight) && (scaleX.x < 0)) || ((!facingRight) && (scaleX.x > 0)))
            scaleX.x *= -1;
        transform.localScale = scaleX;
        // This part actually takes the X scale for the sprite and multiplies it by -1, or in other words causes the sprite to flip horizontally
    }
    
}
