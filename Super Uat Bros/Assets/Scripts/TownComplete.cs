﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TownComplete : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        SceneManager.LoadScene("Level 2");
    }
}
